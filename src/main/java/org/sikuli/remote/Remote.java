package org.sikuli.remote;

import org.openqa.selenium.remote.ExecuteMethod;
import org.sikuli.api.Screen;

public interface Remote {
	ExecuteMethod getExecutionMethod();
	Screen getScreen();
}
