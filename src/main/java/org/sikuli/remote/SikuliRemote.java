package org.sikuli.remote;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.ExecuteMethod;
import org.sikuli.api.Screen;
import org.sikuli.remote.client.RemoteScreen;
import org.sikuli.remote.client.SikuliHttpCommandExecutor;

public class SikuliRemote implements Remote, ExecuteMethod {
	
	
	final private SikuliHttpCommandExecutor httpCommandExecutor; 
	final private Screen remoteScreen;
		
	public SikuliRemote(URL serverUrl){
		httpCommandExecutor= new SikuliHttpCommandExecutor(serverUrl);
		remoteScreen = new RemoteScreen(this);
	}
	
	
	@Override
	public Object execute(String commandName, Map<String, ?> parameters) {
		Command command = new Command(null, commandName, parameters);
		try {
			return httpCommandExecutor.execute(command);
		} catch (IOException e) {
			return null;
		}
	}	

	@Override
	public ExecuteMethod getExecutionMethod() {
		return this;
	}

	@Override
	public Screen getScreen() {
		return remoteScreen;
	}

}
